package bla.bla.bla;

import java.util.Map;

public class AuditDataFactory {

    private final String factoryName;

    public AuditDataFactory(String factoryName) {
        this.factoryName = factoryName;
    }

    public Map<AuditParam, String> getBaseAuditData(Long id, AuditParam... auditParams) {
        return new AuditDataByID(id, auditParams).getAuditData();
    }

    public Map<AuditParam, String> getBaseAuditData(String val1, Integer id, AuditParam... auditParams) {
        return new AuditDataByStringAndID(val1, id, auditParams).getAuditData();
    }

    private class AuditDataByID extends AbstractAuditDataSupplier {

        private final Long id;

        private AuditDataByID(Long id, AuditParam... auditParams) {
            super(auditParams);
            this.id = id;
        }

        private String getIdPlus2() {
            return String.valueOf(id + 2);
        }

        @Override
        protected String getValueByAuditParam(AuditParam auditParam) {
            switch (auditParam) {
                case I_CODE:
                    return "FixCode";
                case RES:
                    return getIdPlus2();
                case ID:
                    return "factoryName = " + factoryName;
                default:
                    throw new UnsupportedOperationException("Unsupported code " + auditParam.getCode());
            }
        }
    }

    private class AuditDataByStringAndID extends AbstractAuditDataSupplier {

        private final Integer id;
        private final String val1;

        private AuditDataByStringAndID(String val1, Integer id, AuditParam... auditParams) {
            super(auditParams);
            this.id = id;
            this.val1 = val1;
        }

        private String getIdPlus4() {
            return String.valueOf(id + 4);
        }

        @Override
        protected String getValueByAuditParam(AuditParam auditParam) {
            switch (auditParam) {
                case I_CODE:
                    return "{" + val1 + "}";
                case RES:
                    return getIdPlus4();
                case FIELD1:
                    return "factoryName(field1) = " + factoryName;
                default:
                    throw new UnsupportedOperationException("Unsupported code " + auditParam.getCode());
            }
        }
    }

}
