package bla.bla.bla;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static bla.bla.bla.AuditParam.*;

public final class EntryPoint {

    private static final Logger log = LoggerFactory.getLogger(EntryPoint.class);

    private EntryPoint() {
        log.info("EntryPoint started");
        AuditDataFactory auditDataFactory1 = new AuditDataFactory("factory one");

        AuditService auditService = new AuditService();
        auditService.auditOperation("testOperName",
                auditDataFactory1.getBaseAuditData(10L, RES, ID));

        auditService.auditOperation("test 3",
                auditDataFactory1.getBaseAuditData(10L, ID),
                auditDataFactory1.getBaseAuditData("who",10, FIELD1, I_CODE, RES));
    }

    public static void main(String[] args) {
        new EntryPoint();
    }
}
