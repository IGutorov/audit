package bla.bla.bla;

import lombok.Getter;

public enum AuditParam {
    FIELD1("f1"), ID("identity"), I_CODE("iCod"), RES("res");

    @Getter
    private final String code;

    AuditParam(String code) {
        this.code = code;
    }
}
