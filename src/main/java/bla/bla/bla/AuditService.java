package bla.bla.bla;

import lombok.extern.slf4j.Slf4j;

import java.util.Map;
import java.util.stream.Stream;

@Slf4j
public class AuditService {

    public void auditOperation(String operationName, Map<AuditParam, String>... data) {
        log.info("start audit operationName = {}", operationName);
        Stream.of(data).forEach(m -> m.forEach((k, v)
                -> log.info("audit. code = {}, value = {}", k.getCode(), v)));
        log.info("complete audit for operationName = {}", operationName);
    }
}
