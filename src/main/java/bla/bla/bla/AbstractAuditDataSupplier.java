package bla.bla.bla;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

abstract class AbstractAuditDataSupplier {

    private final List<AuditParam> auditParams;

    protected AbstractAuditDataSupplier(AuditParam[] auditParams) {
        this.auditParams = Arrays.asList(auditParams);
    }

    protected abstract String getValueByAuditParam(AuditParam auditParam);

    public Map<AuditParam, String> getAuditData() {
        return auditParams.stream().collect(Collectors.toMap(k -> k, this::getValueByAuditParam));
    }
}
